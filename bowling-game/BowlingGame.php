<?php


class BowlingGame
{
    private $rolls = [];
    private $current_roll = 0;

    public function __construct() {}

    public function roll($pins)
    {
        $this->rolls[$this->current_roll++] = $pins;
    }

    public function calculateScore()
    {
        $score = 0;
        $first_roll_in_frame = 0;
        for ($frame = 0; $frame < 10; $frame++) {
            if ($this->isSpare($first_roll_in_frame)) {
                $score += 10 + $this->nextBallForSpare($first_roll_in_frame);
                $first_roll_in_frame += 2;

            } elseif ($this->isStrike($first_roll_in_frame)) {
                $score += 10 + $this->nextTwoBallsForStrike($first_roll_in_frame);
                $first_roll_in_frame += 1;

            } else {
                $score += $this->currentTwoBallsForFrame($first_roll_in_frame);
                $first_roll_in_frame += 2;

            }

        }

        return $score;
    }

    private function isSpare($first_roll_in_frame): bool
    {
        return $this->rolls[$first_roll_in_frame] + $this->rolls[$first_roll_in_frame + 1] === 10;
    }

    private function isStrike(int $first_roll_in_frame): bool
    {
        return $this->rolls[$first_roll_in_frame] === 10;
    }

    private function nextBallForSpare(int $first_roll_in_frame)
    {
        return $this->rolls[$first_roll_in_frame + 2];
    }

    private function nextTwoBallsForStrike(int $first_roll_in_frame)
    {
        return $this->rolls[$first_roll_in_frame+1] + $this->rolls[$first_roll_in_frame+2];
    }

    private function currentTwoBallsForFrame(int $first_roll_in_frame)
    {
        return $this->rolls[$first_roll_in_frame] + $this->rolls[$first_roll_in_frame + 1];
    }
}