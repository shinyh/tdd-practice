<?php
require_once 'BowlingGame.php';

use PHPUnit\Framework\TestCase;

class BowlingGameTest extends TestCase
{
    private $bowling_game;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->bowling_game = new BowlingGame();
    }
    # 핀이 하나도 안쓰러진걸 거터라고 한다

    public function test_거터게임()
    {
        $this->rollMany(20, 0);

        $this->assertEquals(0, $this->bowling_game->calculateScore());
    }

    public function test_매게임_하나만_쓰러트린_경우()
    {
        $this->rollMany(20, 1);

        $this->assertEquals(20, $this->bowling_game->calculateScore());
    }


    public function test_1프레임이_스페어처리한_경우()
    {
        $this->rollSpare();
        $this->bowling_game->roll(3);
        $this->rollMany(17, 0);

        $this->assertEquals(16, $this->bowling_game->calculateScore());
    }

    public function test_1프레임이_스트라이크인_경우()
    {
        $this->rollStrike();
        $this->bowling_game->roll(5);
        $this->bowling_game->roll(3);
        $this->rollMany(16, 0);

        $this->assertEquals(26, $this->bowling_game->calculateScore());
    }

    public function test_퍼펙트게임()
    {
        $this->rollMany(12, 10);
        $this->assertEquals(300, $this->bowling_game->calculateScore());
    }


    private function rollMany($frames, $pins)
    {
        for ($i = 0; $i < $frames; $i++) {
            $this->bowling_game->roll($pins);

        }
    }

    private function rollSpare()
    {
        $this->bowling_game->roll(5);
        $this->bowling_game->roll(5);
    }

    private function rollStrike()
    {
        $this->bowling_game->roll(10);
    }

}
