<?php
require_once 'PrimeFactors.php';

use PHPUnit\Framework\TestCase;

class PrimeFactorsTest extends TestCase
{
    private $prime_factors;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->prime_factors = new PrimeFactors();
    }

    public function test_소인수분해_할수_있는지()
    {
        $this->assertEquals([], $this->prime_factors->factorize(1));
        $this->assertEquals([2], $this->prime_factors->factorize(2));
        $this->assertEquals([3], $this->prime_factors->factorize(3));
        $this->assertEquals([2, 2], $this->prime_factors->factorize(4));
        $this->assertEquals([5], $this->prime_factors->factorize(5));
        $this->assertEquals([2, 3], $this->prime_factors->factorize(6));
        $this->assertEquals([7], $this->prime_factors->factorize(7));
        $this->assertEquals([2, 2, 2], $this->prime_factors->factorize(8));
        $this->assertEquals([3, 3], $this->prime_factors->factorize(9));
        $this->assertEquals([3, 11], $this->prime_factors->factorize(33));

    }
}
