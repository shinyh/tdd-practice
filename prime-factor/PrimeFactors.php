<?php


class PrimeFactors
{
    public function __construct() {}

    public function factorize(int $integer)
    {
        $array = [];

        for ($divisor = 2;$integer > 1; $divisor++)
            for (; $integer% $divisor === 0; $integer /= $divisor)
                array_push($array, $divisor);

        return $array;
    }

}